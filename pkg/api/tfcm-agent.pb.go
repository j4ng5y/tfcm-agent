// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.11.2
// source: api/tfcm-agent.proto

package api

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SystemInformationResponse_OSType int32

const (
	SystemInformationResponse_WINDOWS SystemInformationResponse_OSType = 0
	SystemInformationResponse_LINUX   SystemInformationResponse_OSType = 1
	SystemInformationResponse_DARWIN  SystemInformationResponse_OSType = 2
)

// Enum value maps for SystemInformationResponse_OSType.
var (
	SystemInformationResponse_OSType_name = map[int32]string{
		0: "WINDOWS",
		1: "LINUX",
		2: "DARWIN",
	}
	SystemInformationResponse_OSType_value = map[string]int32{
		"WINDOWS": 0,
		"LINUX":   1,
		"DARWIN":  2,
	}
)

func (x SystemInformationResponse_OSType) Enum() *SystemInformationResponse_OSType {
	p := new(SystemInformationResponse_OSType)
	*p = x
	return p
}

func (x SystemInformationResponse_OSType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SystemInformationResponse_OSType) Descriptor() protoreflect.EnumDescriptor {
	return file_api_tfcm_agent_proto_enumTypes[0].Descriptor()
}

func (SystemInformationResponse_OSType) Type() protoreflect.EnumType {
	return &file_api_tfcm_agent_proto_enumTypes[0]
}

func (x SystemInformationResponse_OSType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SystemInformationResponse_OSType.Descriptor instead.
func (SystemInformationResponse_OSType) EnumDescriptor() ([]byte, []int) {
	return file_api_tfcm_agent_proto_rawDescGZIP(), []int{1, 0}
}

type SystemInformationResponse_OSArch int32

const (
	SystemInformationResponse_AMD64 SystemInformationResponse_OSArch = 0
	SystemInformationResponse_I386  SystemInformationResponse_OSArch = 1
	SystemInformationResponse_ARM   SystemInformationResponse_OSArch = 2
	SystemInformationResponse_ARM64 SystemInformationResponse_OSArch = 3
)

// Enum value maps for SystemInformationResponse_OSArch.
var (
	SystemInformationResponse_OSArch_name = map[int32]string{
		0: "AMD64",
		1: "I386",
		2: "ARM",
		3: "ARM64",
	}
	SystemInformationResponse_OSArch_value = map[string]int32{
		"AMD64": 0,
		"I386":  1,
		"ARM":   2,
		"ARM64": 3,
	}
)

func (x SystemInformationResponse_OSArch) Enum() *SystemInformationResponse_OSArch {
	p := new(SystemInformationResponse_OSArch)
	*p = x
	return p
}

func (x SystemInformationResponse_OSArch) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SystemInformationResponse_OSArch) Descriptor() protoreflect.EnumDescriptor {
	return file_api_tfcm_agent_proto_enumTypes[1].Descriptor()
}

func (SystemInformationResponse_OSArch) Type() protoreflect.EnumType {
	return &file_api_tfcm_agent_proto_enumTypes[1]
}

func (x SystemInformationResponse_OSArch) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SystemInformationResponse_OSArch.Descriptor instead.
func (SystemInformationResponse_OSArch) EnumDescriptor() ([]byte, []int) {
	return file_api_tfcm_agent_proto_rawDescGZIP(), []int{1, 1}
}

type SystemInformationRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *SystemInformationRequest) Reset() {
	*x = SystemInformationRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_tfcm_agent_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SystemInformationRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SystemInformationRequest) ProtoMessage() {}

func (x *SystemInformationRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_tfcm_agent_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SystemInformationRequest.ProtoReflect.Descriptor instead.
func (*SystemInformationRequest) Descriptor() ([]byte, []int) {
	return file_api_tfcm_agent_proto_rawDescGZIP(), []int{0}
}

type SystemInformationResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Ostype   SystemInformationResponse_OSType `protobuf:"varint,1,opt,name=ostype,proto3,enum=tfcmapi.SystemInformationResponse_OSType" json:"ostype,omitempty"`
	Osarch   SystemInformationResponse_OSArch `protobuf:"varint,2,opt,name=osarch,proto3,enum=tfcmapi.SystemInformationResponse_OSArch" json:"osarch,omitempty"`
	Hostname string                           `protobuf:"bytes,3,opt,name=hostname,proto3" json:"hostname,omitempty"`
}

func (x *SystemInformationResponse) Reset() {
	*x = SystemInformationResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_tfcm_agent_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SystemInformationResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SystemInformationResponse) ProtoMessage() {}

func (x *SystemInformationResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_tfcm_agent_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SystemInformationResponse.ProtoReflect.Descriptor instead.
func (*SystemInformationResponse) Descriptor() ([]byte, []int) {
	return file_api_tfcm_agent_proto_rawDescGZIP(), []int{1}
}

func (x *SystemInformationResponse) GetOstype() SystemInformationResponse_OSType {
	if x != nil {
		return x.Ostype
	}
	return SystemInformationResponse_WINDOWS
}

func (x *SystemInformationResponse) GetOsarch() SystemInformationResponse_OSArch {
	if x != nil {
		return x.Osarch
	}
	return SystemInformationResponse_AMD64
}

func (x *SystemInformationResponse) GetHostname() string {
	if x != nil {
		return x.Hostname
	}
	return ""
}

var File_api_tfcm_agent_proto protoreflect.FileDescriptor

var file_api_tfcm_agent_proto_rawDesc = []byte{
	0x0a, 0x14, 0x61, 0x70, 0x69, 0x2f, 0x74, 0x66, 0x63, 0x6d, 0x2d, 0x61, 0x67, 0x65, 0x6e, 0x74,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x74, 0x66, 0x63, 0x6d, 0x61, 0x70, 0x69, 0x22,
	0x1a, 0x0a, 0x18, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66, 0x6f, 0x72, 0x6d, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x22, 0x9e, 0x02, 0x0a, 0x19,
	0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x41, 0x0a, 0x06, 0x6f, 0x73, 0x74,
	0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x29, 0x2e, 0x74, 0x66, 0x63, 0x6d,
	0x61, 0x70, 0x69, 0x2e, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66, 0x6f, 0x72, 0x6d,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x2e, 0x4f, 0x53,
	0x54, 0x79, 0x70, 0x65, 0x52, 0x06, 0x6f, 0x73, 0x74, 0x79, 0x70, 0x65, 0x12, 0x41, 0x0a, 0x06,
	0x6f, 0x73, 0x61, 0x72, 0x63, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x29, 0x2e, 0x74,
	0x66, 0x63, 0x6d, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66,
	0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x2e, 0x4f, 0x53, 0x41, 0x72, 0x63, 0x68, 0x52, 0x06, 0x6f, 0x73, 0x61, 0x72, 0x63, 0x68, 0x12,
	0x1a, 0x0a, 0x08, 0x68, 0x6f, 0x73, 0x74, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x68, 0x6f, 0x73, 0x74, 0x6e, 0x61, 0x6d, 0x65, 0x22, 0x2c, 0x0a, 0x06, 0x4f,
	0x53, 0x54, 0x79, 0x70, 0x65, 0x12, 0x0b, 0x0a, 0x07, 0x57, 0x49, 0x4e, 0x44, 0x4f, 0x57, 0x53,
	0x10, 0x00, 0x12, 0x09, 0x0a, 0x05, 0x4c, 0x49, 0x4e, 0x55, 0x58, 0x10, 0x01, 0x12, 0x0a, 0x0a,
	0x06, 0x44, 0x41, 0x52, 0x57, 0x49, 0x4e, 0x10, 0x02, 0x22, 0x31, 0x0a, 0x06, 0x4f, 0x53, 0x41,
	0x72, 0x63, 0x68, 0x12, 0x09, 0x0a, 0x05, 0x41, 0x4d, 0x44, 0x36, 0x34, 0x10, 0x00, 0x12, 0x08,
	0x0a, 0x04, 0x49, 0x33, 0x38, 0x36, 0x10, 0x01, 0x12, 0x07, 0x0a, 0x03, 0x41, 0x52, 0x4d, 0x10,
	0x02, 0x12, 0x09, 0x0a, 0x05, 0x41, 0x52, 0x4d, 0x36, 0x34, 0x10, 0x03, 0x32, 0x74, 0x0a, 0x11,
	0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x12, 0x5f, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e,
	0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x21, 0x2e, 0x74, 0x66, 0x63, 0x6d,
	0x61, 0x70, 0x69, 0x2e, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66, 0x6f, 0x72, 0x6d,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x74,
	0x66, 0x63, 0x6d, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x49, 0x6e, 0x66,
	0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x42, 0x09, 0x5a, 0x07, 0x70, 0x6b, 0x67, 0x2f, 0x61, 0x70, 0x69, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_api_tfcm_agent_proto_rawDescOnce sync.Once
	file_api_tfcm_agent_proto_rawDescData = file_api_tfcm_agent_proto_rawDesc
)

func file_api_tfcm_agent_proto_rawDescGZIP() []byte {
	file_api_tfcm_agent_proto_rawDescOnce.Do(func() {
		file_api_tfcm_agent_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_tfcm_agent_proto_rawDescData)
	})
	return file_api_tfcm_agent_proto_rawDescData
}

var file_api_tfcm_agent_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_api_tfcm_agent_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_api_tfcm_agent_proto_goTypes = []interface{}{
	(SystemInformationResponse_OSType)(0), // 0: tfcmapi.SystemInformationResponse.OSType
	(SystemInformationResponse_OSArch)(0), // 1: tfcmapi.SystemInformationResponse.OSArch
	(*SystemInformationRequest)(nil),      // 2: tfcmapi.SystemInformationRequest
	(*SystemInformationResponse)(nil),     // 3: tfcmapi.SystemInformationResponse
}
var file_api_tfcm_agent_proto_depIdxs = []int32{
	0, // 0: tfcmapi.SystemInformationResponse.ostype:type_name -> tfcmapi.SystemInformationResponse.OSType
	1, // 1: tfcmapi.SystemInformationResponse.osarch:type_name -> tfcmapi.SystemInformationResponse.OSArch
	2, // 2: tfcmapi.SystemInformation.GetSystemInformation:input_type -> tfcmapi.SystemInformationRequest
	3, // 3: tfcmapi.SystemInformation.GetSystemInformation:output_type -> tfcmapi.SystemInformationResponse
	3, // [3:4] is the sub-list for method output_type
	2, // [2:3] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_api_tfcm_agent_proto_init() }
func file_api_tfcm_agent_proto_init() {
	if File_api_tfcm_agent_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_tfcm_agent_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SystemInformationRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_tfcm_agent_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SystemInformationResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_tfcm_agent_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_tfcm_agent_proto_goTypes,
		DependencyIndexes: file_api_tfcm_agent_proto_depIdxs,
		EnumInfos:         file_api_tfcm_agent_proto_enumTypes,
		MessageInfos:      file_api_tfcm_agent_proto_msgTypes,
	}.Build()
	File_api_tfcm_agent_proto = out.File
	file_api_tfcm_agent_proto_rawDesc = nil
	file_api_tfcm_agent_proto_goTypes = nil
	file_api_tfcm_agent_proto_depIdxs = nil
}

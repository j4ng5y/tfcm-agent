package cmd

import (
	"log"
	"net"

	"github.com/j4ng5y/tfcm-agent/pkg/server"
	"github.com/spf13/cobra"
)

func Run() {
	var (
		configFile    string
		proto, ipaddr string
		port          int
		rootCMD       = &cobra.Command{
			Use:     "tfcm-agent",
			Version: "0.1.0",
			Run: func(ccmd *cobra.Command, args []string) {
				var C *server.Config

				if configFile != "" {
					C = server.NewConfigFromFile(configFile)
				} else {
					p, err := server.NewProtocol(proto)
					if err != nil {
						log.Fatal(err)
					}
					pt, err := server.NewPort(port)
					if err != nil {
						log.Fatal(err)
					}
					C = server.NewConfigWithOptions(
						server.WithProtocol(p),
						server.WithIP(net.ParseIP(ipaddr)),
						server.WithPort(pt))
				}

				S := server.New(server.WithConfig(C))
				S.Run()
			},
		}
	)

	rootCMD.Flags().StringVarP(&configFile, "config", "f", "", "A custom configuration file to use")
	rootCMD.Flags().StringVar(&proto, "protocol", "tcp", "The protocol to run the agent on")
	rootCMD.Flags().StringVar(&ipaddr, "ipaddress", "0.0.0.0", "The ip address to run the agent on")
	rootCMD.Flags().IntVar(&port, "port", 10000, "The port to run the agent on")

	if err := rootCMD.Execute(); err != nil {
		log.Fatal(err)
	}
}

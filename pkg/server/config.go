package server

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"strings"

	"gopkg.in/yaml.v2"
)

type Protocol string

func (P Protocol) isValid() bool {
	switch strings.ToLower(string(P)) {
	case "tcp":
		return true
	default:
		return false
	}
}

func (P Protocol) String() string {
	return string(P)
}

func NewProtocol(p string) (Protocol, error) {
	P := Protocol(p)
	if !P.isValid() {
		return Protocol(""), fmt.Errorf("%s is an invalid protocol type", p)
	}
	return P, nil
}

type Port int

func (P Port) isValid() bool {
	switch {
	case int(P) > 1024 && int(P) <= 65535:
		return true
	default:
		return false
	}
}

func (P Port) Int() int {
	return int(P)
}

func NewPort(p int) (Port, error) {
	P := Port(p)
	if !P.isValid() {
		return Port(0), fmt.Errorf("%d is an invalid port number", p)
	}
	return P, nil
}

type ConfigOption func(*Config)

type GRPCConfig struct {
	Protocol  Protocol `yaml:"protocol"`
	IPAddress net.IP   `yaml:"ipAddress"`
	Port      Port     `yaml:"port"`
}

type TFCMAgentConfig struct {
	GRPC GRPCConfig `yaml:"gRPC"`
}

type Config struct {
	TFCMAgentConfig TFCMAgentConfig `yaml:"tfcmAgentConfig"`
}

func (C Config) ConnectionString() string {
	return fmt.Sprintf("%s:%d", C.TFCMAgentConfig.GRPC.IPAddress.String(), C.TFCMAgentConfig.GRPC.Port.Int())
}

func NewConfigFromFile(filename string) *Config {
	C := &Config{}

	f, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	if err := yaml.Unmarshal(f, C); err != nil {
		log.Fatal(err)
	}
	return C
}

func NewDefaultConfig() *Config {
	proto, _ := NewProtocol("tcp")
	port, _ := NewPort(10000)
	ip := net.ParseIP("0.0.0.0")
	return &Config{
		TFCMAgentConfig: TFCMAgentConfig{
			GRPC: GRPCConfig{
				Protocol:  proto,
				IPAddress: ip,
				Port:      port,
			},
		},
	}
}

func WithProtocol(p Protocol) ConfigOption {
	return func(c *Config) {
		c.TFCMAgentConfig.GRPC.Protocol = p
	}
}

func WithIP(ip net.IP) ConfigOption {
	return func(c *Config) {
		c.TFCMAgentConfig.GRPC.IPAddress = ip
	}
}

func WithPort(p Port) ConfigOption {
	return func(c *Config) {
		c.TFCMAgentConfig.GRPC.Port = p
	}
}

func NewConfigWithOptions(opts ...ConfigOption) *Config {
	proto, _ := NewProtocol("tcp")
	port, _ := NewPort(10000)
	ip := net.ParseIP("0.0.0.0")
	C := &Config{
		TFCMAgentConfig: TFCMAgentConfig{
			GRPC: GRPCConfig{
				Protocol:  proto,
				IPAddress: ip,
				Port:      port,
			},
		},
	}
	for _, opt := range opts {
		opt(C)
	}
	return C
}

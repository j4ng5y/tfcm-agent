package server

import (
	"context"
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"

	"google.golang.org/grpc/metadata"

	"github.com/j4ng5y/tfcm-agent/pkg/api"
)

type SystemInformation struct {
	*api.UnimplementedSystemInformationServer
}

func (S *SystemInformation) getSystemInformation() (*api.SystemInformationResponse, error) {
	var (
		err    error
		ostype api.SystemInformationResponse_OSType
		osarch api.SystemInformationResponse_OSArch
	)
	switch strings.ToUpper(runtime.GOOS) {
	case "LINUX":
		ostype = api.SystemInformationResponse_LINUX
	case "DARWIN":
		ostype = api.SystemInformationResponse_DARWIN
	case "WINDOWS":
		ostype = api.SystemInformationResponse_WINDOWS
	default:
		err = fmt.Errorf("%s is not a supported OS type", runtime.GOOS)
	}
	switch strings.ToUpper(runtime.GOARCH) {
	case "AMD64":
		osarch = api.SystemInformationResponse_AMD64
	case "386":
		osarch = api.SystemInformationResponse_I386
	case "ARM":
		osarch = api.SystemInformationResponse_ARM
	case "ARM64":
		osarch = api.SystemInformationResponse_ARM64
	default:
		err = fmt.Errorf("%s is not a supported OS arch", runtime.GOARCH)
	}
	hostname, err := os.Hostname()

	resp := &api.SystemInformationResponse{
		Ostype:   ostype,
		Osarch:   osarch,
		Hostname: hostname,
	}

	return resp, err
}

func (S *SystemInformation) GetSystemInformation(c context.Context, req *api.SystemInformationRequest) (*api.SystemInformationResponse, error) {
	log.Println(metadata.FromIncomingContext(c))
	return S.getSystemInformation()
}

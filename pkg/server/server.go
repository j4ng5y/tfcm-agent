package server

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"os/signal"
	"time"

	"google.golang.org/grpc/credentials"

	"github.com/j4ng5y/tfcm-agent/pkg/api"
	"google.golang.org/grpc"
)

type ServerOption func(s *Server)

func WithConfig(c *Config) ServerOption {
	return func(s *Server) {
		s.Config = c
	}
}

type Server struct {
	Config *Config
}

func (S *Server) genCA() error {
	var err error
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(20170404),
		Subject: pkix.Name{
			Organization: []string{"TFCM"},
			Country:      []string{"US"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(1, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}
	capriv, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return err
	}

	capub := &capriv.PublicKey
	cacert, err := x509.CreateCertificate(rand.Reader, ca, ca, capub, capriv)
	if err != nil {
		return err
	}

	caOut, err := os.Create("tfcm-ca.crt")
	pem.Encode(caOut, &pem.Block{Type: "CERTIFICATE", Bytes: cacert})
	caOut.Close()

	caKeyOut, err := os.OpenFile("tfcm-ca.key", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	pem.Encode(caKeyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(capriv)})
	caKeyOut.Close()

	return err
}

func (S *Server) genServerCerts() error {
	var err error
	catls, err := tls.LoadX509KeyPair("tfcm-ca.crt", "tfcm-ca.key")
	if err != nil {
		return err
	}
	ca, err := x509.ParseCertificate(catls.Certificate[0])
	if err != nil {
		return err
	}
	serverCert := &x509.Certificate{
		SerialNumber: big.NewInt(20170404),
		Subject: pkix.Name{
			Organization: []string{"TFCM"},
			Country:      []string{"US"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(1, 0, 0),
		SubjectKeyId:          []byte{1, 2, 3, 4, 6},
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature,
		BasicConstraintsValid: true,
		IPAddresses:           []net.IP{net.IPv4(0, 0, 0, 0)},
	}
	priv, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return err
	}
	pub := &priv.PublicKey
	svrCert, err := x509.CreateCertificate(rand.Reader, serverCert, ca, pub, catls.PrivateKey)
	if err != nil {
		return err
	}

	certOut, err := os.Create("tfcm-server.crt")
	if err != nil {
		return err
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: svrCert})
	certOut.Close()

	keyOut, err := os.OpenFile("tfcm-server.key", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})
	keyOut.Close()

	return nil
}

func (S *Server) genCerts() error {
	if err := S.genCA(); err != nil {
		return err
	}
	if err := S.genServerCerts(); err != nil {
		return err
	}
	return nil
}

func New(opts ...ServerOption) *Server {
	S := &Server{
		Config: NewDefaultConfig(),
	}
	for _, opt := range opts {
		opt(S)
	}
	if err := S.genCerts(); err != nil {
		log.Fatal(err)
	}

	return S
}

func (S *Server) Run() {
	runChan := make(chan (os.Signal), 1)

	lis, err := net.Listen(S.Config.TFCMAgentConfig.GRPC.Protocol.String(), S.Config.ConnectionString())
	if err != nil {
		log.Fatal(fmt.Errorf("failed to start the gRPC server: %+v", err))
	}

	cert, err := tls.LoadX509KeyPair("tfcm-server.crt", "tfcm-server.key")
	if err != nil {
		log.Fatal(err)
	}

	config := &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth:   tls.NoClientCert,
	}

	grpcServer := grpc.NewServer(grpc.Creds(credentials.NewTLS(config)))
	api.RegisterSystemInformationServer(grpcServer, &SystemInformation{})

	signal.Notify(runChan, os.Interrupt)

	log.Printf("starting the gRPC server on: %s", S.Config.ConnectionString())
	go grpcServer.Serve(lis)

	sig := <-runChan

	log.Printf("stopping the gRPC server due to %s", sig.String())
	grpcServer.GracefulStop()
}

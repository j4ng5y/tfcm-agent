all: protobuf-compile test build

.PHONY: protobuf-compile
protobuf-compile:
	@protoc \
	--go_out=. \
	--go-grpc_out=. \
	api/tfcm-agent.proto

.PHONY: test
test:
	@go test -coverprofile=cover.out ./... -race

.PHONY: build
build:
	@go build -a -o bin/tfcm-agent cmd/tfcm-agent/tfcm-agent.go